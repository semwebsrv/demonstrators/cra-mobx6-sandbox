import React from 'react'
import { useStore } from 'stores/stores'
import { observer } from 'mobx-react'

export const Controller = () => {

  const store = useStore();

  console.log("User object: %o",store.sessionStore.user);

  return (
   <p>
     This is the controller
     <button onClick={() => store.sessionStore.increment()}>Increment</button>
     <button onClick={() => store.sessionStore.decrement()}>Decrement</button>
   </p>
  )
}

export default observer(Controller)

