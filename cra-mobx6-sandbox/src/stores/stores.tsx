import React from 'react'

import SessionStore from './SessionStore'

export const stores = {
  sessionStore: new SessionStore()
};

export const StoreContext = React.createContext(stores);

export const useStore = () => {
  const store = React.useContext(StoreContext)
  if (!store) {
    // this is especially useful in TypeScript so you don't need to be checking for null all the time
    throw new Error('useStore must be used within a StoreProvider.')
  }
  return store
}
