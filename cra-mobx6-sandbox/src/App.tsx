import React from 'react';
import './App.css';
import { useStore } from 'stores/stores'
import Controller from 'components/Controller'
import { observer } from 'mobx-react'

function App() {

  const store = useStore()

  return (
    <div className="App">
      <header className="App-header">
        <p>
          Current value of counter : {store.sessionStore.counter}
        </p>
        <Controller/>
      </header>
    </div>
  );
}

export default observer(App);
