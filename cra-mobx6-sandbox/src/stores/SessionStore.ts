import { makeObservable, action, observable } from "mobx"

export default class SessionStore {

  user = {};
  counter = 0;

  constructor() {
    console.log("SessionStore::constructor");
    makeObservable(this, {
      user: observable,
      counter: observable,
      setUser: action,
      increment: action,
      decrement: action
    })
  }

  setUser(user: object) {
    console.log('setuser(%o)', user)
    this.user = user
  }

  increment() {
    console.log("increment %d",this.counter);
    this.counter++
  }

  decrement() {
    console.log("decrment %d",this.counter);
    this.counter--
  }
}

